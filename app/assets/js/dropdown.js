$(".dropdown dt a").click(function() {
    $(this).parents(':eq(1)').find("ul").toggle();
});


$(".dropdown dd a").click(function() {
    var text = $(this).html();
    $(this).parents(':eq(4)').find("dt a span").html(text).css('color', 'white');
    $(this).parents(':eq(4)').find('.dropdown + input').val(text)
    $(".dropdown dd ul").hide();
});

$(document).bind('click', function(e) {
    var $clicked = $(e.target);
    if (! $clicked.parents().hasClass("dropdown"))
        $(".dropdown dd ul").hide();
});
