// $('.modal').hide();

$('.button.cancel, .button.primary').click(function() {
  $('.modal').hide();
});
$('#showSynchroModal').click(function() {
  $("#synchroModal").show();
});
$('#showTestingModal').click(function() {
  $("#testingModal").show();
});
$('#openAddModuleMobile').click(function() {
  $("#addModuleMobile").show();
});
$('#openAddModulePlanner').click(function() {
  $("#addModulePlanner").show();
});
$('#openAddModuleReviews').click(function() {
  $("#addModuleReviews").show();
});
$('#openAddSiteModal').click(function() {
  $("#addSiteModal").show();
});
$('#openAddModuleNewsletter').click(function() {
  $("#addModuleNewsletter").show();
});
$('#openDeleteModal').click(function() {
  $("#deleteModal").show();
});
$('#openSynchroConfirmModal').click(function() {
  $("#synchroConfirmModal").show();
});
